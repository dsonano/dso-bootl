/*
 * Copyright 2012 Tormod Volden
 *
 * Demonstration of download-to-RAM-and-run
 *
 * The program itself requires the DS0201 DFU bootloader (USB)
 * in order to run. To download it to RAM an extended bootloader
 * must be installed on the DSO Nano, for instance bootl from
 * https://gitorious.org/dsonano/dso-bootl
*/

#include "stm32f10x_lib.h"
#include "hw_config.h"

#define INITIAL_STACK 0x20004ff0

/* declare the entry function */
void hello (void);

/* Just enough of a NVIC vector to convince the boot loader */
__attribute__ ((section(".vector_table")))
void (* const chainload_vector[])(void) =
	{ (void (*)(void)) INITIAL_STACK, hello };


/* NOTE! Variables will not be initialized unless startup code is added.
   A simple hack is to declare variables as volatile and initialize
   them explicitly */

void hello (void)
{
	/* enable GPIOA so that we can check more buttons */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | 
		RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);
	/* The ports must be configured /after/ they are clocked
           so GPIO_Config is called again to configure GPIOA */
	GPIO_Config();

	LCD_Initial();
	Clear_Screen(0);
	Display_Logo(30, 150);
	Display_Str(8, 87, 0x07E0, 0, "   Hello world! ");
	Display_Str(8, 53, 0x07FF, 1, " RAM loader demonstration");

	Display_Str(8, 23, 0x07FF, 1, " Press RIGHT to reboot...");
	/* Wait for RIGHT key to be pressed */
	while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)) {
		Delay(10);
	}
	Display_Str(8, 23, 0x07FF, 1, " Goodbye for now!        ");
	/* Wait for key to be released */
	while (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)) {
		Delay(10);
	}
	Delay(1000);
	Reset_Device();
}
