/*
 * Copyright 2012 Tormod Volden
 *
 * Demonstration applet for download-to-RAM-and-run
 *
 * This program scans through all pages of flash memory
 * and detects applications (or what looks like it) and
 * lets the user choose which one to run.
 *
 * To build: make NAME=appselect

 * The program itself requires the DS0201 DFU bootloader (USB)
 * in order to run. To download it to RAM an extended bootloader
 * must be installed on the DSO Nano, for instance bootl from
 * https://gitorious.org/dsonano/dso-bootl
*/

#include "stm32f10x_lib.h"
#include "hw_config.h"

#define INITIAL_STACK 0x20004ff0

/* declare the entry function */
void appselect (void);

/* Just enough of a NVIC vector to convince the boot loader */
__attribute__ ((section(".vector_table")))
void (* const chainload_vector[])(void) =
	{ (void (*)(void)) INITIAL_STACK, appselect };


/* sprintf(buffer, "%08x", number) */
void sprint_08hex(char *buffer, int number)
{
	unsigned char digit;
	int i;

	for (i = 0; i < 8; i++) {
		digit = (number & 0xf);
		if (digit > 9)
			digit += '@' - 9;
		else
			digit += '0';
		buffer[7-i] = digit;
		number >>= 4;
	}
	buffer[8] = 0;
}

#define FLASH_START 0x08000000
#define FLASH_SIZE 0x20000
#define RAM_SIZE 0x5000

int is_vector_table(u32* vector)
{
	if (vector[1] > FLASH_START &&
	    vector[1] < FLASH_START + FLASH_SIZE &&
	    vector[0] > 0x20000000 &&
	    vector[0] < 0x20000000 + RAM_SIZE)
		return 1;
	else
		return 0;
}

/* for signaling the extended boot loader */
#define RAM_MAGIC 0x5aba5aba
#define RAM_MAGIC_LOCATION 0x20004ff8

void setup_boot_magic(u32 vector)
{
    *((u32*) RAM_MAGIC_LOCATION) = RAM_MAGIC;
    *((u32*) RAM_MAGIC_LOCATION + 1) = vector;
}

#define KEY_UP    0x01
#define KEY_DOWN  0x02
#define KEY_OK    0x04
#define KEY_LEFT  0x08
#define KEY_RIGHT 0x10
#define KEY_A     0x20
#define KEY_B     0x40

/* Global variables will not be initialized unless startup code is added.
   Set them explicitly inside a function instead of here */

int old_keys;

int get_keys(void)
{
	/* static int old_keys = 0; * made global for explicit initialization */
	int keys_pressed, newly_pressed;

	keys_pressed =  (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6)?0:KEY_UP) +
			(GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_9)?0:KEY_DOWN) +
			(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5)?0:KEY_LEFT) +
			(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)?0:KEY_RIGHT) +
			(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4)?0:KEY_A) +
			(GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_11)?0:KEY_OK) +
			(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3)?0:KEY_B);
	newly_pressed = keys_pressed & ~old_keys;
	old_keys = keys_pressed;
	return newly_pressed;
}

#define MAX_APPS 20

void select_app(void)
{
	u32 app_address[MAX_APPS];
	int found=0;
	int choice = 0;
	char s[9];
	u32 address;
	int i;
	int keys = 0;

	old_keys = 0;
	/* may also read out flash size (store end) from registers */
	for (address = FLASH_START; address < FLASH_START + FLASH_SIZE; address += 1024) {
		if (is_vector_table((u32*) address))
			app_address[found++] = address;
		if (found == MAX_APPS)
			break;
	}

	for (i=0; i<found; i++) {
		sprint_08hex(s, app_address[i]);
		Display_Str(20, 180 - i * 16, 0x07e0, 0, s);
	}
	while (!(keys & KEY_OK)) {
		if (keys & KEY_DOWN)
			if (++choice >= found)
				choice = 0;
		if (keys & KEY_UP)
			if (--choice < 0)
				choice = found - 1;
		Display_Str(12, 180 - choice * 16, 0x7e0, 0, ">");
		while (!(keys = get_keys())) {};
		Display_Str(12, 180 - choice * 16, 0x7e0, 0, " ");
	}
	setup_boot_magic(app_address[choice]);
}

void appselect (void)
{
	/* enable GPIOA so that we can check more buttons */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | 
		RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);
	/* The ports must be configured /after/ they are clocked
           so GPIO_Config is called again to configure GPIOA */
	GPIO_Config();

	LCD_Initial();
	Clear_Screen(0);
	Display_Logo(30, 150);
	Display_Str(8, 87, 0x07E0, 0, "   App selector ");
	Display_Str(8, 53, 0x07FF, 1, " RAM loader demonstration");

	Display_Str(8, 23, 0x07FF, 1, " Press OK to select application");

	select_app();

	Display_Str(8, 23, 0x07FF, 1, " Press RIGHT to reboot...      ");
	/* Wait for RIGHT key to be pressed */
	while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)) {
		Delay(10);
	}
	Display_Str(8, 23, 0x07FF, 1, " Goodbye for now!        ");
	/* Wait for key to be released */
	while (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)) {
		Delay(10);
	}
	Delay(1000);
	Reset_Device();
}
