/*
 * Copyright 2012 Tormod Volden
 *
 * Demonstration of download-to-RAM-and-run
 * This program displays the internal temperature sensor
 * reading in a large font.
 *
 * To build:
 *   make NAME=tempsens EXTRA_OBJS=stm32f10x_adc.o
 *
 * The program itself requires the DS0201 DFU bootloader (USB)
 * in order to run. To download it to RAM an extended bootloader
 * must be installed on the DSO Nano, for instance bootl from
 * https://gitorious.org/dsonano/dso-bootl
*/

#include "stm32f10x_conf.h"
#define _ADC
#define _ADC1
#include "stm32f10x_lib.h"
#include "hw_config.h"
#include "usb_regs.h"

/* include stm32f10x_adc.c directly, to keep Makefile unchanged? */

#define INITIAL_STACK 0x20004ff0

/* declare the entry function */
void bigfonts (void);

/* Just enough of a NVIC vector to convince the boot loader */
__attribute__ ((section(".vector_table")))
void (* const chainload_vector[])(void) =
	{ (void (*)(void)) INITIAL_STACK, bigfonts };


/* NOTE! Variables will not be initialized unless startup code is added.
   A simple hack is to declare variables as volatile and initialize
   them explicitly */


#define Char_Dot ((const unsigned short *) 0x8002af4)

void Display_Str_Scaled(int scale, unsigned short x0, unsigned short y0, unsigned short Color, unsigned char Mode, unsigned const char *s)
{
  signed short i, j, b, bh; 
  int n,m;
  Point_SCR(x0, y0);
  for (j = 0; j < 14 * scale; ++j) { 
    Set_Pixel(Mode?Color:0);
  }
  x0++;        
  while (*s!=0) {
    unsigned const short *scanline = Char_Dot + ((*s-0x22)*8);
    for(i = 0; i < 8; ++i) {
      if((*s == 0x20) || (*s == 0x21))
		bh=0x0000;
      else
                bh=scanline[i];
      for (n = 0; n < scale; n++) {
	      Point_SCR(x0 + i*scale + n, y0);
	      if((*s==0x21) && (i == 4))
		break;
	      b = bh;
	      for(j=0;j<14;++j){
		for (m = 0; m < scale; m++)
			  Set_Pixel(((b&4)?Mode:!Mode)?0:Color);
		b >>= 1;
	      }
      }
    }
    if(*s==0x21)
	x0 += (4 * scale);       
    else
	x0 += (8 * scale);             
    ++s;	/* next character */
  }
}

/* Prints integers up to 4 decimal digits 
   Printed string is always 4 characters + e terminator */
void sprint_int(char *s, int number)
{
	int m, digit;
	int div = 10000;
	int leadingzero = 1;

	for (m = 0; m < 5; m++) {
		digit = number / div;
		if (digit > 9) {
			s[0] = 'X'; s[1] = 0;
			return;
		}
		if (leadingzero && digit == 0 && div != 1) {
			s[m] = ' ';
		} else {
			s[m] = '0' + digit;
			leadingzero = 0;
		}
		number -= div * digit;
		div /= 10;
	}
	s[5] = '\0';
}

void ADC_Configuration(void)
{
  ADC_InitTypeDef  ADC_InitStructure;
  /* PCLK2 is the APB2 clock */
  /* ADCCLK = PCLK2/6 = 72/6 = 12MHz*/
  /* MOVED! RCC_ADCCLKConfig(RCC_PCLK2_Div6); */

  /* Enable ADC1 clock so that we can talk to it */
  /* MOVED! RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE); */
  /* Put everything back to power-on defaults */
  /* REPLACED! ADC_DeInit(ADC1); */

  /* ADC1 Configuration ------------------------------------------------------*/
  /* ADC1 and ADC2 operate independently */
  ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
  /* Disable the scan conversion so we do one at a time */
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  /* Don't do contimuous conversions - do them on demand */
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  /* Start conversin by software, not an external trigger */
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  /* Conversions are 12 bit - put them in the lower 12 bits of the result */
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  /* Say how many channels would be used by the sequencer */
  ADC_InitStructure.ADC_NbrOfChannel = 1;

  /* Now do the setup */
  ADC_Init(ADC1, &ADC_InitStructure);
  /* Enable ADC1 */
  ADC_Cmd(ADC1, ENABLE);

  /* Enable ADC1 reset calibaration register */
  ADC_ResetCalibration(ADC1);
  /* Check the end of ADC1 reset calibration register */
  while(ADC_GetResetCalibrationStatus(ADC1));
  /* Start ADC1 calibaration */
  ADC_StartCalibration(ADC1);
  /* Check the end of ADC1 calibration */
  while(ADC_GetCalibrationStatus(ADC1));
}

u16 readADC1()
{
  // Start the conversion
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  // Wait until conversion completion
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
  // Get the conversion value
  return ADC_GetConversionValue(ADC1);
}

void bigfonts (void)
{
	int m, a, t;
	char num_s[8];

	/* dsoloader enables USB on every reset, try turning it off 	
	   so that the host realize there is no DFU etc */
	DFU_Reset();
	Delay(100);
	_SetCNTR(CNTR_FRES);
	_SetISTR(0);
	USB_Cable_Config (DISABLE);
	_SetCNTR(CNTR_FRES + CNTR_PDWN);
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, DISABLE);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);
	/* enable GPIOA so that we can check more buttons */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | 
		RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE |
		RCC_APB2Periph_ADC1, ENABLE);
	/* The ports must be configured /after/ they are clocked
           so GPIO_Config is called again to configure GPIOA */
	GPIO_Config();

	/* This is RCC_APB2PeriphResetCmd Enable/Disable */
	RCC->APB2RSTR |= RCC_APB2Periph_ADC1;
	RCC->APB2RSTR &= ~RCC_APB2Periph_ADC1;

	ADC_Configuration();
	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_16, 1, ADC_SampleTime_239Cycles5);

	LCD_Initial();
	Clear_Screen(0);
	Display_Logo(30, 150);
	Display_Str(8, 80, 0x07E0, 0, "   Hello world! ");
	Display_Str(8, 53, 0x07FF, 1, " RAM loader demonstration");

	for (m=100; m >= 0; m--) {
		sprint_int(num_s, m);
		Display_Str_Scaled(6, 4, 140, 0x07E0, 0, num_s);
	}

	Display_Str(8, 53, 0x07E0, 0, "                         ");

	for (m=1; m < 4; m++)
		Display_Str_Scaled(m, 10, -30+m*41, 0x07E0, 0, "DSO Nano");

	Display_Str_Scaled(6, 260, 140, 0x07E0, 0, "C");
	
	Display_Str(8, 23, 0x07FF, 1, " Press RIGHT to reboot...");
	/* Wait for RIGHT key to be pressed */
	while (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)) {
		a = readADC1();
		/* Print raw ADC value */
		sprint_int(num_s, a);
		Display_Str_Scaled(3, 188, 40, 0x07E0, 0, num_s);
		/* roughly convert to degrees Celcius */
		t = (4*25 + 1480 - a*3300/4096)/4;
		sprint_int(num_s, t);
		Display_Str_Scaled(6, 4, 140, 0x07E0, 0, num_s);
		Delay(500);
	}
	Display_Str(8, 23, 0x07FF, 1, " Goodbye for now!        ");
	/* Wait for key to be released */
	while (!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)) {
		Delay(10);
	}
	Delay(1000);
	
	_SetCNTR(0);
	Reset_Device();
}
