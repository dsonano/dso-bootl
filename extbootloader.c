/*
 * Copyright 2012 Tormod Volden
 *
 * Adds download-to-RAM functionality to the DSO Nano bootloader
*/

#include "spi_flash.h"
#include "usb_lib.h"
#include "hw_config.h"
#include "usb_conf.h"
#include "usb_prop.h"
#include "usb_desc.h"
#include "usb_pwr.h"

/* These "belong" to the original bootloader */
extern u8  Load_Buffer[wTransferSize]; /* RAM Buffer for Downloaded Data */
extern u32 wBlockNum, wlength;
extern u32 Manifest_State;
extern u32 Pointer;  /* Base Address to Erase, Program or Read */
extern DEVICE_PROP Device_Property;
extern u8 DeviceState;
extern u8 DeviceStatus[];
extern ONE_DESCRIPTOR DFU_String_Descriptor[6];
extern void USB_Init(void);

/* Replacement functions and strings */
void DFU_Status_Out_Memory(void);
const u8 DFU_StringInterface0_Corrected[DFU_SIZ_STRING_INTERFACE0];
const u8 DFU_StringInterface1_Memory[DFU_SIZ_STRING_INTERFACE1];

/* Where the default application is located */
/* This bootloader needs to use the 0x08004000 location in order
   to be launched by the original bootloader so until we have
   come up with a better solution, other apps should start at
   the next page or somewhere higher */

#define APP_START 0x08007800

/* To signal a boot application selection across a device reset */
#define RAM_MAGIC 0x5aba5aba

/* For other, future applications performing a boot selection via this
   mechanism, this might be the top of the stack. However that should
   not pose any problems if some care is taken */
#define RAM_MAGIC_LOCATION 0x20004ff8
/* The word following the magic should point to the NVIC vector table
   of application that should be started after reset */

/* Since we are not calling startup code, variables are not initialized
 * Therefore mark them volatile and initialize them explicitly */
static volatile int wrote_magic;

/* original bootloader must chainload this function */
__attribute__ ((section(".extended_boot_loader_entry")))
void extended_boot_loader (void)
{
    u32 *vector_table;

    /* TODO: do magic check after button check, to let someone
       upload to RAM without jumping to it ? */

    /* Check RAM magic for boot selection */
    if (*((u32*) RAM_MAGIC_LOCATION) == RAM_MAGIC)
    {
	/* invalidate magic to avoid any infinite reset loops */
	*((u32*) RAM_MAGIC_LOCATION) = 0;
	vector_table = (u32*) *((u32*) RAM_MAGIC_LOCATION + 1);
	__MSR_MSP(vector_table[0]);
	((void (*)(void)) vector_table[1])();
    }

    /* Check button */
    /* If button "OK" is not pressed, jump to default app */
    /* Note that the default app can not simply sit in 0x80004000
     * since we need to overwrite the reset handler of any app
     * located there, in order to chainload this bootloader. */

    if (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_11)) {
	vector_table = (u32*) APP_START;
	__MSR_MSP(vector_table[0]);
	((void (*)(void)) vector_table[1])();
    }

    /* TODO: make a cludge for BenF, scan for APP/LIB interface vector? */

    /* Future features, can also be put in a second stage in high memory:
       1. Display a menu, where RAM loading is one option
       2. Scan through pages for more apps by checking valid
       stack pointer and reset handler */


    /* Start USB DFU boot loader with memory download support */

    LCD_Initial();
    Clear_Screen(0);
    Display_Logo(30, 150);
    Display_Str(8, 87, 0x07E0, 0, "   Please Connect to computer");
    Display_Str(8, 23, 0x07FF, 1, "DS0201 Extended DFU version 2.0");

    /* Change device property to use new Status Out function */
    Device_Property.Process_Status_OUT = DFU_Status_Out_Memory;

    /* Fix up flash memory map reported in string descriptor,
       this will also protect this code itself which is between
       0x8003000 and 0x8004000 */
    DFU_String_Descriptor[4].Descriptor = (u8*)DFU_StringInterface0_Corrected;

    /* Replace alternate interface 1 description with RAM memory map */
    DFU_String_Descriptor[5].Descriptor = (u8*)DFU_StringInterface1_Memory;

    /* Maybe change Manufacturer/Product string for recognition ? */

    /* The original boot loader starts with these values */
    DeviceState     = STATE_dfuERROR;
    DeviceStatus[0] = STATUS_ERRFIRMWARE;
    DeviceStatus[4] = DeviceState;

    wrote_magic = 0;

    USB_Init();

    Display_Str(8, 87, 0x07E0, 0, "         USB connected...    ");

    while (1);
}

/* This is based on DFU_Status_Out() from the ST DFU example */

/***********************************************************************
* Function Name  : DFU_Status_Out_Memory.
* Description    : DFU status OUT routine enhanced for memory downloads.
* Input          : None.
* Output         : None.
* Return         : None.
***********************************************************************/

void DFU_Status_Out_Memory (void)
{
  DEVICE_INFO *pInfo = &Device_Info;
  u32 i, Addr, DATA, pages;

  if (pInfo->USBbRequest == DFU_GETSTATUS)
  {
    if (DeviceState == STATE_dfuDNBUSY)
    {
      if (wBlockNum == 0)   /* Decode the Special Command*/
      {
        if ((Load_Buffer[0] ==  CMD_GETCOMMANDS) && (wlength == 1))
        {
        }
        else if  (( Load_Buffer[0] ==  CMD_SETADDRESSPOINTER ) && (wlength == 5))
        {
          Pointer  = Load_Buffer[1];
          Pointer += Load_Buffer[2] << 8;
          Pointer += Load_Buffer[3] << 16;
          Pointer += Load_Buffer[4] << 24;
	  /* write first address into RAM */
	  if (!wrote_magic)
	  {
	    *((u32*) RAM_MAGIC_LOCATION) = RAM_MAGIC;
	    *((u32*) RAM_MAGIC_LOCATION + 1) = Pointer;
	    wrote_magic = 1;
	  }
        }
        else if (( Load_Buffer[0] ==  CMD_ERASE ) && (wlength == 5))
        {
          Pointer  = Load_Buffer[1];
          Pointer += Load_Buffer[2] << 8;
          Pointer += Load_Buffer[3] << 16;
          Pointer += Load_Buffer[4] << 24;

	  if  (Pointer >= 0x20000000) /* Internal RAM */
	  {
	    /* no need to erase */
	  }
	  /* Note that this is 0x800000 (8GB) in the original!
	     Not sure if it is a typo or intentional. */
          else if  (Pointer > 0x08000000)  /* Internal Flash */
          {
            Internal_FLASH_PageErase(Pointer);
          }
          else
          {
            SPI_FLASH_SectorErase(Pointer);
          }
        }
      }

      else if (wBlockNum > 1)  // Download Command
      {
        Addr = ((wBlockNum - 2) * wTransferSize) + Pointer;
	if  (Pointer >= 0x20000000) /* Internal RAM */
	{
	  for (i = 0; i < wlength; i += 4)
		*((u32*) (Addr + i)) = *((u32*) &Load_Buffer[i]);
	}
	/* Note that this is 0x800000 in the original! */
        else if (Pointer > 0x08000000)   /* Internal flash programming */
        {
          if  (wlength & 0x3) /* Not an aligned data */
          {
            for (i = wlength; i < ((wlength & 0xFFFC) + 4); i++)
            {
              Load_Buffer[i] = 0xFF;
            }
          }
          /* Data received are Word multiple */

          for (i = 0; i <  wlength; i = i + 4)
          {
            DATA = *((u32*) & Load_Buffer[i]);
            Internal_FLASH_WordWrite(Addr, DATA);
            Addr += 4;
          }
        }
        else    /* SPI Flash programming by Page of 256 Bytes */
        {
          pages = (((wlength & 0xFF00)) >> 8);
          if  (wlength & 0xFF) /* Not a 256 aligned data */
          {
            for ( i = wlength; i < ((wlength & 0xFF00) + 0x100) ; i++)
            {
              Load_Buffer[i] = 0xFF;
            }
            pages = (((wlength & 0xFF00)) >> 8 ) + 1;
          }
          for (i = 0; i < pages; i++)
          {
            SPI_FLASH_PageWrite(&Load_Buffer[i*256], Addr, 256);
            Addr += 0x100;
          }
        }
      }
      wlength = 0;
      wBlockNum = 0;

      DeviceState =  STATE_dfuDNLOAD_SYNC;
      DeviceStatus[4] = DeviceState;
      DeviceStatus[1] = 0;
      DeviceStatus[2] = 0;
      DeviceStatus[3] = 0;
      return;
    }
    else if (DeviceState == STATE_dfuMANIFEST) /* Manifestation in progress*/
    {
      DFU_write_crc();
      return;
    }
  }
  return;
}


/* TODO: verify those lengths in original bootloader binary */
/* Changed to protect 16 instead of 12 first pages */
const u8 DFU_StringInterface0_Corrected[DFU_SIZ_STRING_INTERFACE0] =
  {
    DFU_SIZ_STRING_INTERFACE0,
    0x03,
    // Interface 0: "@Internal Flash   /0x08000000/16*001Ka,112*001Kg"
    '@', 0, 'I', 0, 'n', 0, 't', 0, 'e', 0, 'r', 0, 'n', 0, 'a', 0, 'l', 0,  /* 18 */
    ' ', 0, 'F', 0, 'l', 0, 'a', 0, 's', 0, 'h', 0, ' ', 0, ' ', 0, /* 16 */

    '/', 0, '0', 0, 'x', 0, '0', 0, '8', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, /* 22 */

    '/', 0, '1', 0, '6', 0, '*', 0, '0', 0, '0', 0, '1', 0, 'K', 0, 'a', 0, /* 18 */
    ',', 0, '1', 0, '1', 0, '2', 0, '*', 0, '0', 0, '0', 0, '1', 0, 'K', 0, 'g', 0, /* 20 */
  };

/* Describes memory map for RAM downloading and uploading */
/* Marks the first 3 KB which the bootloader is using as read-only */
const u8 DFU_StringInterface1_Memory[DFU_SIZ_STRING_INTERFACE1] =
  {
    DFU_SIZ_STRING_INTERFACE1,
    0x03,
    // Interface 1: "@RAM                 /0x20000000/003*001Ka,017*001Ke"
    '@', 0, 'I', 0, 'n', 0, 't', 0, 'e', 0, 'r', 0, 'n', 0, 'a', 0, 'l', 0,
    ' ', 0, 'R', 0, 'A', 0, 'M', 0, ' ', 0, ' ', 0, ' ', 0, ' ', 0, ' ', 0, ' ', 0,
    '/', 0, '0', 0, 'x', 0, '2', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0', 0,
    '/', 0, '0', 0, '3', 0, '*', 0, '0', 0, '0', 0, '1', 0, 'K', 0, 'a', 0,
    ',', 0, '1', 0, '7', 0, '*', 0, '0', 0, '0', 0, '1', 0, 'K', 0, 'e', 0
  };

#define ORIGINAL_SP_INIT 0x20000a7c

/* Just enough of a NVIC vector to convince the original boot loader */
/* For testing only */
/* Do not include this if there is a patched app/lib at 08004000 */
/*
__attribute__ ((section(".chainload_vector_table")))
void (* const chainload_vector[])(void) =
	{ (void (*)(void)) ORIGINAL_SP_INIT, extended_boot_loader };
*/
