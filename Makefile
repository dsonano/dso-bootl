
NAME = bootl

#EWARM = "wine C:/Program Files/IAR Systems/..."
#CC = $(EWARM)/arm/bin/iccarm.exe
#LD = $(EWARM)/common/bin/xlink.exe
#LINKSCRIPT = linker_arm_flash.xcl
#XLINK_FLAGS = -f $(LINKSCRIPT) -map

CC = arm-none-eabi-gcc
LD = arm-none-eabi-ld
CP = arm-none-eabi-objcopy
DUMP = arm-none-eabi-objdump
LINKSCRIPT = gnu-linker.ld

MFLAGS  = -mcpu=cortex-m3 -mthumb
ASFLAGS = $(MFLAGS)
CFLAGS  = $(MFLAGS) $(INCLUDES) -c -fno-common -g -Os
LDFLAGS = -T $(LINKSCRIPT) -Map=$(NAME).map

INCLUDES = -I../DS0201_OpenSource/USBLib/inc \
	   -I../DS0201_OpenSource/library/inc \
	   -I../DS0201_OpenSource/DS0201_DFU/include

OBJS = extbootloader.o dsoloader-func.o dsoloader-var.o

all: $(NAME).bin $(NAME).dis

GENERATED = dsoloader-func.S dsoloader-var.S

dsoloader-func.S: dsoloader-func.list
	awk 'BEGIN{printf ".thumb\n.syntax unified\n.code 16\n"} { printf ".globl %s\n.thumb_func\n. = 0x%08x\n%s:\n\n", $$2, strtonum("0x"$$1)-0x08000000, $$2 }' $< > $@
dsoloader-var.S: dsoloader-var.list
	awk 'BEGIN{printf ".thumb\n.syntax unified\n"} { printf ".globl %s\n. = 0x%08x\n%s:\n\n", $$2, strtonum("0x"$$1)-0x20000000, $$2 }' $< > $@

$(NAME).bin:	$(NAME).elf
	$(CP) -O binary $< $@

$(NAME).dis:	$(NAME).elf
	$(DUMP) -S $< > $@

$(NAME).elf: $(LINKSCRIPT) $(OBJS)
	$(LD) -o $@ $(LDFLAGS) $(XLINK_FLAGS) $(OBJS)

clean:
	rm -f $(OBJS) $(GENERATED) \
	      $(NAME).elf $(NAME).bin $(NAME).dis $(NAME).map
